//
//  PersonListTableViewCell.swift
//  OLXChallenge
//
//  Created by Tarek Abdala on 12/02/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit
import Kingfisher

class PersonListTableViewCell: UITableViewCell {
    @IBOutlet var poster: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var desc: UILabel!
    @IBOutlet var button: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        button.layer.cornerRadius = 13.0
        button.layer.borderColor = button.backgroundColor?.cgColor
        button.setTitleColor(button.backgroundColor, for: .normal)
        button.backgroundColor = UIColor.white
        button.layer.borderWidth = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bind(with p: Person) {
        title!.text = p.name
        desc!.text = "\(p.distance!)m"
        let url = URL(string: p.imageUrl!)
        poster?.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.3))], progressBlock: nil, completionHandler: nil)
        poster.layer.cornerRadius = poster.frame.size.height/2
    }
    
    //
    // MARK: - IBActions
    
    @IBAction func connect(_ sender: Any) {
    }

}
