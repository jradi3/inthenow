//
//  PersonMapper.swift
//  fh
//
//  Created by Tarek Jradi on 28/08/2017.
//  Copyright © 2017 Tarek Jradi. All rights reserved.
//

import Foundation
import ObjectMapper

class PersonResponse : Mappable {
    var list: [Person]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        list <- map["people"]
    }
}

class Person : Mappable {
    var id: String?
    var name: String?
    var distance: Int?
    var imageUrl: String?
    var lat: Float?
    var lng: Float?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        distance <- map["distance"]
        imageUrl <- map["imageUrl"]
        lat <- map["lat"]
        lng <- map["lng"]
    }
}
