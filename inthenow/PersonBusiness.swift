//
//  PersonBusiness.swift
//  inthenow
//
//  Created by Tarek Jradi on 28/08/2017.
//  Copyright © 2017 Tarek Jradi. All rights reserved.
//

import Foundation

public class PersonBusiness : NSObject {
    
    //
    // MARK: - Properties
    
    let service = PersonService()
    
    //
    // MARK: - .GET (Functions Data Request)
    
    /**
     get a list of person
     
     - returns: list of people or failure.
     */
    
    func getProfiles(success : @escaping (Array<Person>) -> (), failure  : ((_ error : Error) -> ())?) {
        service.getProfiles(success: { (result) in
            success(result)
        }, failure: { error in
            failure!(error)
        })
    }
    
}
