//
//  ViewController.swift
//  inthenow
//
//  Created by Tarek Jradi on 26/08/2017.
//  Copyright © 2017 Genial Tech. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON
import CoreLocation

class GLPresentViewController1 : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

class ViewController: GLViewPagerViewController,GLViewPagerViewControllerDataSource,GLViewPagerViewControllerDelegate, CLLocationManagerDelegate {
    
    // MARK: - cache properties
    var viewControllers: NSArray = NSArray()
    var tabTitles: NSArray = NSArray()
    let ps = PersonService()

    var progress = 0.0

    
    let locationManager = CLLocationManager()
    var userLocation = CLLocation()

    
    func determineMyCurrentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    
    
    

    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0] as CLLocation
        loadList(lat: "\(userLocation.coordinate.latitude)", lng: "\(userLocation.coordinate.longitude)")
        self.locationManager.stopUpdatingLocation()
    }

    
    
    
    func timerFire(_ timer: Timer) {
        progress += (timer.timeInterval/5)
        SwiftSpinner.show(progress: progress, title: "Searching people around you")
        if progress >= 1 {
            timer.invalidate()
            SwiftSpinner.show(duration: 2.0, title: "\(DataSingleton.shared.people.count) people around", animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        determineMyCurrentLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SwiftSpinner.sharedInstance.outerColor = UIColor.white.withAlphaComponent(0.5)
        SwiftSpinner.sharedInstance.innerColor = UIColor.white.withAlphaComponent(0.5)
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerFire), userInfo: nil, repeats: true)
    }
    
    func loadList(lat : String, lng: String){
        
        //let la = "aa"
        ps.get(endPoint: "search?lat=\(lat)&lng=\(lng)&r=30", success: { (result) in
            if DataSingleton.shared.people.count == 0 {
                for (_,subJson):(String, JSON) in result {
                    let dict = subJson
                    let person = Person()
                    person.id = dict["id"].string
                    person.name = dict["name"].string
                    person.distance = dict["distance"].int
                    person.imageUrl = dict["imageUrl"].string
                    person.lat = dict["lat"].float
                    person.lng = dict["lng"].float
                    DataSingleton.shared.people.append(person)
                }
                self.loadViewControllers()
            }
        }) { (error) in
            self.alertMessage(title: "Somethig wrong", message: "Check your intenet connection or reinstall application")
        }
    }
    
    func loadViewControllers(){
        self.navigationItem.title = "Paged Tabs"
        self.setDataSource(newDataSource: self)
        self.setDelegate(newDelegate: self)
        self.padding = 0
        self.leadingPadding = 0
        self.trailingPadding = 0
        self.defaultDisplayPageIndex = 1
        self.tabAnimationType = GLTabAnimationType.GLTabAnimationType_WhileScrolling
        self.indicatorColor = UIColor.init(colorLiteralRed: 255.0 / 255.0, green: 23.0 / 255.0 , blue: 104.0/255.0, alpha: 1.0)
        self.supportArabic = false
        self.fixTabWidth = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc1 = storyboard.instantiateViewController(withIdentifier: "GLPresentViewController1") as! GLPresentViewController1
        let vc2 = storyboard.instantiateViewController(withIdentifier: "MapListViewController") as! MapListViewController
        let vc3 = storyboard.instantiateViewController(withIdentifier: "PersonViewController") as! PersonViewController
        
        self.viewControllers = [vc1,
                                vc2,
                                vc3
        ]
        
        self.tabTitles = [ "In The Now",
                           "Map" ,
                           "People"]
    }

    // MARK: - GLViewPagerViewControllerDataSource
    func numberOfTabsForViewPager(_ viewPager: GLViewPagerViewController) -> Int {
        return self.viewControllers.count
    }
    
    func viewForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIView {
        let label:UILabel = UILabel.init()
        label.text = self.tabTitles.object(at: index) as? String
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        label.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        label.font = UIFont(name: "Arial", size: 13)
        return label
    }
    
    func contentViewControllerForTabAtIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIViewController {
        return self.viewControllers.object(at: index) as! UIViewController
    }
    
    // MARK: - GLViewPagaerViewControllerDelegate
    func didChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int) {
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        prevLabel.textColor = UIColor.darkGray
        currentLabel.textColor = UIColor.darkGray
        prevLabel.font = UIFont.systemFont(ofSize: 13.0)
        currentLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
    }
    
    func willChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int, progress: CGFloat) {
        if fromTabIndex == index {
            return;
        }
        
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0 - (0.1 * progress), y: 1.0 - (0.1 * progress))
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9 + (0.1 * progress), y: 0.9 + (0.1 * progress))
        currentLabel.textColor =  UIColor.init(colorLiteralRed: Float(0.3 + 0.2 * progress), green: Float(0.3 - 0.3 * progress), blue: Float(0.3 + 0.2 * progress), alpha: 1.0)
        prevLabel.textColor = UIColor.init(colorLiteralRed: Float(0.5 - 0.2 * progress), green: Float(0.0 + 0.3 * progress), blue: Float(0.5 - 0.2 * progress), alpha: 1.0)
    }
    
    func widthForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> CGFloat {
        let prototypeLabel:UILabel = UILabel.init()
        prototypeLabel.text = self.tabTitles.object(at: index) as? String
        prototypeLabel.textAlignment = NSTextAlignment.center
        prototypeLabel.font = UIFont.systemFont(ofSize: 16.0)
        return prototypeLabel.intrinsicContentSize.width
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
