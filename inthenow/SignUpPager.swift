//
//  SignUpPager.swift
//  inthenow
//
//  Created by Tarek Jradi on 05/09/2017.
//  Copyright © 2017 Genial Tech. All rights reserved.
//

import UIKit

class SignUpPager: UIViewController, UIPageViewControllerDataSource {
    
    // MARK: - Variables
    private var pageViewController: UIPageViewController?
    
    // Initialize it right away here
    private let contentImages = ["Icon-1024",
                                 "Icon-1024"]
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        disableScroll()
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(SignUpPager.goToPage1), userInfo: nil, repeats: false)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpPager.goToPage2), name:Notification.Name(rawValue: "goToPage2"), object: nil)
    }
    
    private func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        pageViewController = pageController
        pageController.setViewControllers([UIViewController()], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
        appearance.backgroundColor = UIColor.gray
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController is PageItemController1 {
            return self.storyboard!.instantiateViewController(withIdentifier: "ItemController2") as! PageItemController2
        }
        if viewController is PageItemController2 {
            return nil
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController is PageItemController1 {
            return pageViewController.viewControllers?[0] as! PageItemController1
        }
        if viewController is PageItemController2 {
            return pageViewController.viewControllers?[1] as! PageItemController2
        }
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 2
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    // MARK: - Additions
    
    func disableScroll(){
        for view in (self.pageViewController?.view.subviews)! {
            if view is UIScrollView {
                let v = view as! UIScrollView
                v.isScrollEnabled = false
            }
        }
    }

    func goToPage1() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ItemController1") as! PageItemController1
        pageViewController!.setViewControllers([vc], direction: .forward, animated: true, completion: nil)
    }

    func goToPage2() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ItemController2") as! PageItemController2
        pageViewController!.setViewControllers([vc], direction: .forward, animated: true, completion: nil)
    }

}
