//
//  Service.swift
//  inthenow
//
//  Created by Tarek Jradi on 27/08/2017.
//  Copyright © 2017 Tarek Jradi. All rights reserved.
//

import Alamofire

public class Service : NSObject {
    
    //
    // MARK: - Properties
    
    static var manager: SessionManager {
        get {
            let m = SessionManager.default
            return m
        }
    }
}

public class APIUrls {
    static let prefixMockable: String = "http://demo4964089.mockable.io/"
    static let prefix: String = "http://api.power-of-now.club/"
    static var base_url_image:String = "http://lorempixel.com/"
}
