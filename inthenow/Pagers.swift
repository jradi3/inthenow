//
//  Pagers.swift
//  inthenow
//
//  Created by Tarek Jradi on 06/09/2017.
//  Copyright © 2017 Genial Tech. All rights reserved.
//


import UIKit

class PageItemController1: UIViewController {
    
    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var activity: UIActivityIndicatorView!
    let service = PersonService()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        phoneNumber.perform(
            #selector(becomeFirstResponder),
            with: nil,
            afterDelay: 0.1
        )
    }
    
    @IBAction func register(_ sender: UIButton) {
        view.endEditing(true)
        
        if (phoneNumber.text?.characters.count)! < 9 {
            phoneNumber.shake()
            return
        }
        activity.startAnimating()
        service.post(parameters: ["phoneNumber":phoneNumber.text!], endPoint: "registration/signup", success: { (response) in
            if response["message"] == "Phone Number already registered" {
                self.appDelegate.switchLoginSuccessful()
                DataSingleton.shared.phoneNumber = self.phoneNumber.text!
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToPage2"), object: nil)
            DataSingleton.shared.phoneNumber = self.phoneNumber.text!
        }) { (error) in
            self.activity.stopAnimating()
            sender.shake()
        }
    }
    
}

class PageItemController2: UIViewController, UITextFieldDelegate {
    
    // MARK: - @IBOutlets

    @IBOutlet var numbers: [UITextField]!
    @IBOutlet var activity: UIActivityIndicatorView!

    // MARK: - let, var

    let service = PersonService()
    var indexNumber = 0
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.becomeFirstResponder()
        focusTextField(index: 0)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var i = 0
        for field in numbers {
            //delete all numbers
            if isBackSpace(string: string) {
                focusTextField(index: 0)
                field.text = "-"
                continue
            }
            //new number
            if field.text == "-" {
                indexNumber = field.tag
                numbers[i].text = ""; numbers[i].text = string; break;
            }
            //last number inputed
            if i == 4 {
                numbers[5].text = ""; numbers[5].text = string;
                self.view.endEditing(true)
                activity.startAnimating()
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { (timer) in
                    self.validadeNumber()
                })
            }
            i += 1
        }
        return false
    }
    
    func focusTextField(index : Int){
        numbers[index].perform(
            #selector(becomeFirstResponder),
            with: nil,
            afterDelay: 0.1
        )
    }
    
    func isBackSpace(string:String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        return false
    }
    
    func validadeNumber(){
        var number = String()
        for field in numbers {number = number + field.text!}
        service.post(parameters: ["code":number, "phoneNumber": DataSingleton.shared.phoneNumber], endPoint: "registration/activate", success: { (response) in
            if response["token"].description == "null"{
                self.view.shake()
                self.activity.stopAnimating()
                self.focusTextField(index: 0)
            }else {
                let defaults = UserDefaults.standard
                defaults.set(response["token"].description, forKey: "token")
                defaults.synchronize()
                self.appDelegate.switchLoginSuccessful()
            }
        }) { (error) in
            self.view.shake()
            self.activity.stopAnimating()
        }
    }
}
