//
//  DocumentService.swift
//  WizzioConference
//
//  Created by Tarek Jradi on 11/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import Alamofire
import SwiftyJSON

public class DocumentService : Service {
    
    func postImage(_ image: UIImage,
                   _ type: String,
                   _ closure: @escaping (_ progress: Double) -> (),
                   _ success: @escaping () -> (),
                   _ failure: @escaping ((_ error : Error) -> ())
        ) {
        let imageData = UIImageJPEGRepresentation(image, 1)!
        let parameters = [
            "id": String(describing: InfoSystem.processId!),
            "type" : type
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:"\(GlobalUrl.API_URL)images") { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    closure(progress.fractionCompleted)
                })
                upload.responseJSON { response in
                    print("Success")
                    success()
                }
                break
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                break
            }
        }
    }
    
    func post(parameters: Parameters, endPoint : String,
                success: @escaping ((_ result : JSON) -> ()),
                failure: @escaping ((_ error : Error) -> ())){
                Alamofire.request("\(GlobalUrl.API_URL)\(endPoint)", method: .post,
                                  parameters: parameters,
                                  encoding: JSONEncoding.default).responseJSON { (response) in
                    if !(response.result.value is NSNull) {
                        if response.result.error != nil {
                            failure(response.result.error!)
                        } else {
                            success(JSON(response.result.value!))
                        }
                    }
                }
    }
    
    func delete(endPoint : String,
                success: @escaping ((_ result : JSON) -> ()),
                failure: @escaping ((_ error : Error) -> ())){
                Alamofire.request("\(GlobalUrl.API_URL)\(endPoint)", method: .delete).responseJSON { (response) in
                    if !(response.result.value is NSNull) {
                        if response.result.error != nil {
                            failure(response.result.error!)
                        } else {
                            success(JSON(response.result.value!))
                        }
                    }
                }
    }

    
}
