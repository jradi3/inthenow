//
//  UIImage+Helper.swift
//  WizzioConference
//
//  Created by Tarek Jradi on 19/04/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

extension UIImage {
    func resizeImage(scale : CGFloat) -> UIImage {
        let newSize = CGSize.init(width: self.size.width/scale, height: self.size.height/scale)
        var newImage = UIImage()
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
