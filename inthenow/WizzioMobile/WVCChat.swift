//
//  WVCChat.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 28/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//
//  WVC Wizzio Video Conference

import UIKit
import ReactiveCocoa

class WVCChat: UIViewController {
    
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        registerCollabWebRTCClient()
        WebSocketService.shared.connect()
        WebSocketService.shared.addHandlers()
    }
    
    //
    // MARK: - IBActions
    
    @IBAction func connect(_ sender: Any) {
        print("process ID ---------------------> \(InfoSystem.processId!)")
        present(CollabWebRTCClient.sharedInstance().createVideoCallViewController(forNumber: "56565",
                                                                      withCalleeDisplayName: "Client"),
                                                                                   animated: true,
                                                                                 completion: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        let alert = UIAlertController(title: "Cancelar processo", message: "esse processo será cancelado e descartado", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Voltar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { action in
            switch action.style{
            case .default:
                self.appDelegate.switchSession()
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    func getWebRTCAccount() -> WebRTCAccount {
        return WebRTCAccount.init(sipConnectorSocketAddress: GlobalUrl.SIP_CONNECTOR,
                                  sipUsername: "",
                                  sipPassword: "",
                                  andSipDomain: GlobalUrl.SIP_DOMAIN)
    }
    
    func registerCollabWebRTCClient(){
        let account = self.getWebRTCAccount()
        CollabWebRTCClient.sharedInstance().register(account)
        CollabWebRTCClient.sharedInstance().connectStateChangeSignal.subscribeNext { (object) in }
    }
}
