//
//  WebRTCAccount.m
//  CollabSampleWebRTC
//
//  Created by Collab Apple on 03/01/2017.
//  Copyright © 2017 Collab Apple. All rights reserved.
//

#import "WebRTCAccount.h"

@interface WebRTCAccount ()

@property (nonatomic, strong, readwrite) NSString *sipConnectorSocketAddress;
@property (nonatomic, strong, readwrite) NSString *sipUsername;
@property (nonatomic, strong, readwrite) NSString *sipPassword;
@property (nonatomic, strong, readwrite) NSString *sipDomain;

@end

@implementation WebRTCAccount

-(instancetype) initWithSipConnectorSocketAddress:(NSString *)socketAddress sipUsername:(NSString *)sipUsename sipPassword:(NSString *)sipPassword andSipDomain:(NSString *)sipDomain {
 
    if(self = [super init]) {
        self.sipConnectorSocketAddress = socketAddress;
        self.sipUsername = sipUsename;
        self.sipPassword = sipPassword;
        self.sipDomain = sipDomain;
    }
    
    return self;
}

@end
