//
//  PINConfirm.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 25/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

class PINConfirm : UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        UINib(nibName: "PINConfirm", bundle: nil).instantiate(withOwner: self, options: nil)
        addSubview(view)
        self.addSubview(indicator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func processPIN(){
        label1.isHidden = true
        label2.isHidden = true
        indicator.hidesWhenStopped = true
        indicator.center = self.view.center
        indicator.startAnimating()
    }
    
    public func showResult(){
        //label1.isHidden = false
        label2.isHidden = false
        label2.text = "Autorizado!"
        indicator.stopAnimating()
    }
    
}
