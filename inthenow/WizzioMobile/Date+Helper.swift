//
//  Date+Helper.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 20/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
