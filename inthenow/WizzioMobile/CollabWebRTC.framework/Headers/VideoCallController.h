//
//  VideoCallController.h
//  WebRTC_OneContact
//
//  Created by Collab on 02/12/15.
//  Copyright © 2015 onecontactpbx. All rights reserved.
//
#import <UIKit/UIKit.h>

@protocol TakePhotoListener <NSObject>

-(void) onPhotoCompleted:(UIImage *)photo;

@end

__attribute__((visibility("default")))
@interface VideoCallController : UIViewController

-(instancetype) initView;
-(BOOL) videoCallViewToggleFlash;
-(void) videoCallViewSwitchCamera:(BOOL)activateBackCamera;
-(void) videoCallViewTakePhoto;
-(void) setTakePhotoDelegate:(id<TakePhotoListener>)delegate;

@end
