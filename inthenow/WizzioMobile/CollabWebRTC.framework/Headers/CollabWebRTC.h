//
//  CollabWebRTC.h
//  CollabWebRTC
//
//  Created by Collab on 06/05/16.
//  Copyright © 2016 onecontactpbx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectorSipConnector.h"
#import "VideoCallController.h"
#import "ChatSessionController.h"
#import "PushNotificationController.h"
#import "ChatInteractionProvider.h"

//! Project version number for CollabWebRTC.
FOUNDATION_EXPORT double CollabWebRTCVersionNumber;

//! Project version string for CollabWebRTC.
FOUNDATION_EXPORT const unsigned char CollabWebRTCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CollabWebRTC/PublicHeader.h>


