//
//  Header.h
//  WebRTC_OneContact
//
//  Created by Ricardo Domingues on 7/7/17.
//  Copyright © 2017 onecontactpbx. All rights reserved.
//

#import <UIKit/UIKit.h>

__attribute__((visibility("default")))
@interface ChatInteractionProvider : NSObject

+(NSString *) getCurrentChatSession;

@end
