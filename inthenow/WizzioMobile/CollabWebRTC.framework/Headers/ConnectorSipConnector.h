//
//  ConnectorSipConnector.h
//  WebRTC_OneContact
//
//  Created by Collab on 09/05/16.
//  Copyright © 2016 onecontactpbx. All rights reserved.
//
#import <Foundation/Foundation.h>

@class RACSignal;
@class RACDisposable;

__attribute__((visibility("default")))
@interface ConnectorSipConnector : NSObject

@property (nonatomic, readonly) RACSignal *receivePackageConnectedSignal;
@property (nonatomic, readonly) RACSignal *receivePackageRegisteredSignal;
@property (nonatomic, readonly) RACSignal *receivePackageResponseToCallSignal;
@property (nonatomic, readonly) RACSignal *receivePackageRequestToCallSignal;
@property (nonatomic, readonly) RACSignal *receivePackageMessageShutdownCallSignal;
@property (nonatomic, readonly) RACSignal *receivePackageError;
@property (nonatomic, readonly) RACDisposable *receivePackageSignalDispose;

@property (nonatomic, readwrite) NSString *sipUser;
@property (nonatomic, readwrite) NSString *sipPassword;
@property (nonatomic, readwrite) NSString *initator;
@property (nonatomic, readwrite) NSString *target;
@property (nonatomic, readwrite) NSString *targetDisplayName;
@property (nonatomic, readwrite) NSString *chatWebSocketAddress;
@property (nonatomic, readwrite) NSString *wsURL;
@property (nonatomic, readwrite) NSString *nameImageBackground;

@property (nonatomic, readwrite) NSData *pushNotificicationCertificate;
@property (nonatomic, readwrite) NSString *pushNotificationCertificatePassword;
@property (nonatomic, readwrite) BOOL pushNotificationSandbox;

@property (nonatomic, readwrite) NSData *deviceToken;

@property (nonatomic, readwrite) NSString *sessionID;
@property (nonatomic, readwrite) NSString *sdpReceived; //se tiver preenchido no response final envia um type answer, se tiver vazio envia um offer
@property (nonatomic, readwrite) NSString *sdpSend;

@property (nonatomic, readwrite) BOOL audioOnly;
@property (nonatomic, readonly) BOOL registered;
@property (nonatomic, readonly) BOOL connected;

+ (instancetype)sharedInstance;
-(void)connect:(NSString *)wsUrl;
-(void)disconnect;

@end
