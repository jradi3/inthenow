//
//  UIChatSessionController.h
//  WebRTC_OneContact
//
//  Created by Collab Apple on 16/05/2017.
//  Copyright © 2017 onecontactpbx. All rights reserved.
//

#import <UIKit/UIKit.h>

__attribute__((visibility("default")))
@interface ChatSessionController : UIViewController

-(instancetype) initView;

+(ChatSessionController *) newViewForSessionId:(NSString *)sessionId;

@end
