//
//  PushNotificationController.h
//  WebRTC_OneContact
//
//  Created by Ricardo Domingues on 7/5/17.
//  Copyright © 2017 onecontactpbx. All rights reserved.
//
#import <UIKit/UIKit.h>

__attribute__((visibility("default")))
@interface PushNotificationController : NSObject

+(void) receiveNewPushNotificationWithUserInfo:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

@end
