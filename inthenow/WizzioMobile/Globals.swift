//
//  Globals.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 23/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import AnimatedTextInput
//
// MARK: - Enums

enum loginError: Error {
    case ErrorToken
}

//
// MARK: - Structs

struct GlobalUrl {
    static var BASE_URL_IMAGE = ""
    static var API_URL = "https://wizziovc.azurewebsites.net/api/"
    static var WEB_SOCKET_URI = "https://wizziovcbo.azurewebsites.net"
    static var SIP_CONNECTOR = "wss://novabase-test.nubitalk.com:8080"
    static var SIP_DOMAIN = "172.21.0.9:5060"
}

struct InfoSystem {
    static var IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
    static var UDID = UIDevice.current.identifierForVendor?.uuidString
    static var processId = UserDefaults.standard.value(forKey: "processId")
}

struct ScreenSize {
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}

struct GlobalColorSystem {
    static let refA =  UIColor(colorLiteralRed: 32/255, green: 64/255, blue: 87/255, alpha: 1.0) //#204057
    static let refB =  UIColor(colorLiteralRed: 0/255, green: 87/255, blue: 155/255, alpha: 1.0) //#00579B
    static let refC =  UIColor(colorLiteralRed: 0/255, green: 172/255, blue: 193/255, alpha: 1.0) //#00ACC1
    static let refD =  UIColor(colorLiteralRed: 42/255, green: 193/255, blue: 215/255, alpha: 1.0) //#2AC1D7
    static let refE =  UIColor(colorLiteralRed: 152/255, green: 216/255, blue: 229/255, alpha: 1.0) //#98D8E5
    static let refF =  UIColor(colorLiteralRed: 69/255, green: 91/255, blue: 100/255, alpha: 1.0) //#455B64
    static let refG =  UIColor(colorLiteralRed: 115/255, green: 143/255, blue: 157/255, alpha: 1.0) //#738F9D
    static let refH =  UIColor(colorLiteralRed: 161/255, green: 179/255, blue: 188/255, alpha: 1.0) //#A1B3BC
    static let refI =  UIColor(colorLiteralRed: 189/255, green: 201/255, blue: 207/255, alpha: 1.0) //#BDC9CF
    static let refJ =  UIColor(colorLiteralRed: 216/255, green: 223/255, blue: 227/255, alpha: 1.0) //#D8DFE3
    static let refL =  UIColor(colorLiteralRed: 240/255, green: 243/255, blue: 244/255, alpha: 1.0) //#F0F3F4
}

struct CustomTextInputStyle: AnimatedTextInputStyle {
    let activeColor = GlobalColorSystem.refC
    let inactiveColor = GlobalColorSystem.refC.withAlphaComponent(1.0)
    let lineInactiveColor = UIColor.gray.withAlphaComponent(0.6)
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 14.5)
    let textInputFontColor = GlobalColorSystem.refF
    let placeholderMinFontSize: CGFloat = 12
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 20
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 0
    let yHintPositionOffset: CGFloat = 3
    let yPlaceholderPositionOffset: CGFloat = 0
    let textAttributes: [String: Any]? = nil
}

struct NotificationSocket {
    static let ROTATE_CAMERA = Notification.Name("ROTATE_CAMERA")
    static let CONFIRM_OTP = Notification.Name("CONFIRM_OTP")
    static let LIGHT_CONTROL = Notification.Name("LIGHT_CONTROL")
    static let TAKE_PICTURE = Notification.Name("TAKE_PICTURE")
    static let SEND_PICTURE_CONF = Notification.Name(rawValue: "SEND_PICTURE_CONF")
}

struct SocketCommand {
    static let AUTHENTICATE = "AUTHENTICATE"
    static let TAKE_PICTURE = "TAKE_PICTURE"
    static let CONFIRM_OTP = "CONFIRM_OTP"
}

struct SocketType {
    static let RESPONSE = "RESPONSE"
}
