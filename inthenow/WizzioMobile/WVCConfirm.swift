//
//  WVCConfirm.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 21/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

class WVCConfirm: UIViewController {
    
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        WebSocketService.shared.disconnect()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //
    // MARK: - IBActions
    
    @IBAction func close(_ sender: Any) {
        self.appDelegate.switchSession()
    }
    
}
