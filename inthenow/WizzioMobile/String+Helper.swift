//
//  String+Helper.swift
//  WizzioConference
//
//  Created by Tarek Jradi on 28/04/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

extension String {
    func currency() -> String {
        let price = Int(self)! as NSNumber
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.locale = Locale(identifier: "pt_PT")
        return formatter.string(from: price)!
    }
}

extension NSMutableAttributedString{
    func setColorForText(_ textToFind: String, with color: UIColor) {
        let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range.location != NSNotFound {
            addAttribute(NSForegroundColorAttributeName, value: color, range: range)
        }
    }
}
