//
//  WebRTCAccount.h
//  CollabSampleWebRTC
//
//  Created by Collab Apple on 03/01/2017.
//  Copyright © 2017 Collab Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebRTCAccount : NSObject

@property (nonatomic, strong, readonly) NSString *sipConnectorSocketAddress;
@property (nonatomic, strong, readonly) NSString *sipUsername;
@property (nonatomic, strong, readonly) NSString *sipPassword;
@property (nonatomic, strong, readonly) NSString *sipDomain;

-(instancetype) initWithSipConnectorSocketAddress:(NSString *)socketAddress sipUsername:(NSString *)sipUsename sipPassword:(NSString *)sipPassword andSipDomain:(NSString *)sipDomain;

@end
