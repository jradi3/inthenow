//
//  CustomViews.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 30/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

class GradientView : UIView {
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        let locations: [CGFloat] = [ 0.0, 0.5]
        let colors = [GlobalColorSystem.refB.cgColor,
                      GlobalColorSystem.refD.cgColor]
        let colorspace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: colorspace, colors: colors as CFArray, locations: locations)
        var startPoint = CGPoint()
        var endPoint =  CGPoint()
        startPoint.x = 0.0
        startPoint.y = 0.0
        endPoint.x = 800
        endPoint.y = 800
        context!.drawLinearGradient(gradient!,
                                    start: startPoint, end: endPoint, options: .drawsAfterEndLocation)
    }
}
