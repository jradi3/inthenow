//
//  CollabWebRTCClient.h
//  CollabSampleWebRTC
//
//  Created by Collab Apple on 03/01/2017.
//  Copyright © 2017 Collab Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WebRTCAccount;
@class UIViewController;
@class RACSignal;

@interface CollabWebRTCClient : NSObject

@property (nonatomic, readonly) RACSignal *connectStateChangeSignal;

@property (nonatomic, readonly) BOOL isConnected;
@property (nonatomic, strong, readonly) WebRTCAccount *webRTCAccount;

+(instancetype) sharedInstance;

-(void) registerAccount:(WebRTCAccount *)account;

-(UIViewController *) createAudioCallViewControllerForNumber:(NSString *)number withCalleeDisplayName:(NSString *)displayName;
-(UIViewController *) createVideoCallViewControllerForNumber:(NSString *)number withCalleeDisplayName:(NSString *)displayName;


@end
