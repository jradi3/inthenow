//
//  WebSocketService.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 03/08/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit
import SocketIO

class WebSocketService : NSObject {
    
    //
    //MARK: - Properties

    let socket = SocketIOClient(socketURL: URL(string: "\(GlobalUrl.WEB_SOCKET_URI)")!, config: [.log(true), .compress])
    static let shared = WebSocketService()

    //
    // MARK: - Initializers
    
    override init() {
        super.init()
    }
    
    //
    // MARK: Connect, Disconnect Methods
    
    func connect(){
        socket.connect()
    }
    
    func disconnect(){
        socket.disconnect()
    }
    
    //
    // MARK: Socket Handlers
    
    func addHandlers(){
        //will be called on any event, since its useful for debugging the API.
        self.socket.onAny {
            print("Got event: \($0.event), with items: \($0.items!)")
        }
        
        //Default Confirm connected from server
        self.socket.on("connect") { data, ack in
            self.emitAuthenticate()
        }

        //Others Listners
        self.socket.on(NotificationSocket.TAKE_PICTURE.rawValue) { data, ack in
            NotificationCenter.default.post(name: NotificationSocket.TAKE_PICTURE, object: data)
        }
        
        self.socket.on(NotificationSocket.LIGHT_CONTROL.rawValue) {  data, ack in
            NotificationCenter.default.post(name: NotificationSocket.LIGHT_CONTROL, object: nil)
        }
        
        self.socket.on(NotificationSocket.CONFIRM_OTP.rawValue) {  data, ack in
            NotificationCenter.default.post(name: NotificationSocket.CONFIRM_OTP, object: nil)
        }
        
        self.socket.on(NotificationSocket.ROTATE_CAMERA.rawValue) {  data, ack in
            NotificationCenter.default.post(name: NotificationSocket.ROTATE_CAMERA, object: nil)
        }
    }
    
    //
    // MARK: Emit commands
    
    private func emitAuthenticate(){
        let dict = [
            "processId": InfoSystem.processId!,
            ]
        socket.emit(SocketCommand.AUTHENTICATE, dict)
    }
    
    func emitImageSend(result : String){
        let dict = [
            "processId": InfoSystem.processId!,
            "type" : SocketType.RESPONSE,
            "payload" : [
                    "command" : SocketCommand.TAKE_PICTURE,
                    "_id" : result,
                ]
            ]
        socket.emit(SocketType.RESPONSE, dict)
    }

    func emitPIN(number : String){
        let dict = [
            "processId": InfoSystem.processId!,
            "type" : SocketType.RESPONSE,
            "payload" : [
                "command" : SocketCommand.CONFIRM_OTP,
                "code" : number,
            ]
        ]
        socket.emit(SocketType.RESPONSE, dict)
    }

}
