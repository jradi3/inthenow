//
//  Service.swift
//  WizzioConference
//
//  Created by Tarek Jradi on 11/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import Alamofire

public class Service : NSObject {
    
    //
    // MARK: - Properties
    
    static var manager: SessionManager {
        get {
            let m = SessionManager.default
            return m
        }
    }
}
