//
//  WVCForm.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 28/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//
//  WVC Wizzio Video Conference

import AnimatedTextInput
import IHKeyboardAvoiding
import Alamofire

class WVCForm: UIViewController, AnimatedTextInputDelegate {

    //
    // MARK: - Properties
    
    var dataSent = false

    //
    // MARK: - IBOutlets
    
    @IBOutlet weak var name: AnimatedTextInput!
    @IBOutlet weak var email: AnimatedTextInput!
    @IBOutlet weak var password: AnimatedTextInput!
    @IBOutlet weak var nif: AnimatedTextInput!
    @IBOutlet weak var phone: AnimatedTextInput!
    
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureViewController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //
    // MARK: - Configurations
    
    func configureViewController(){
        viewWithDismissKeyboard()
        KeyboardAvoiding.avoidingView = view
        configureFields()
    }
    
    // Fields functions
    
    func configureFields(){
        //Configure text fields
        configure(field: name, with: "Nome")
        configure(field: email, with: "Email")
        configure(field: password, with: "Password")
        configure(field: nif, with: "NIF")
        configure(field: phone, with: "Telemóvel")
        email.type = .email
        password.type = .password
        nif.type = .numeric
        phone.type = .numeric
    }
    
    func configure(field: AnimatedTextInput, with title : String){
        field.accessibilityLabel = "standard_text_input"
        field.placeHolderText = title
        field.style = CustomTextInputStyle()
        field.delegate = self
    }

    //
    // MARK: - AnimatedTextInputDelegate
    
    func animatedTextInputDidBeginEditing(animatedTextInput: AnimatedTextInput){
        if animatedTextInput == name || animatedTextInput == email {
            KeyboardAvoiding.paddingForCurrentAvoidingView = -110
        }else {
            KeyboardAvoiding.paddingForCurrentAvoidingView = -70
        }
    }
    
    func validate() -> Bool {
        for view in self.view.subviews {
            if view is AnimatedTextInput {
                let field = view as! AnimatedTextInput
                if field.text?.characters.count == 0 {return false}
            }
        }
        return true
    }
    
    func getParameters() -> Parameters {
        let parameters: Parameters = [
            "name": String(describing: name.text!),
            "email": String(describing: email.text!),
            "password": String(describing: password.text!),
            "nif": String(describing: nif.text!),
            "phone": String(describing: phone.text!)
        ]
        return parameters
    }
    
}
