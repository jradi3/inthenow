//
//  UINavigationBar+Helper.swift
//  WizzioConference
//
//  Created by Tarek Jradi on 18/04/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

extension UINavigationController
{
    func applyWizzioBarColors(){
        self.navigationBar.barTintColor = UIColor.init(patternImage: UIImage(named: "gradientNavigationBar")!)
        self.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Lato", size: 15)!,
                                                  NSForegroundColorAttributeName: UIColor.white]
        self.navigationBar.backItem?.title = " "
        self.navigationBar.tintColor = UIColor.white
    }
}
