//
//  WVCSegmentContainer.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 31/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//
//  Ref: jeffaburt/UIPageViewController-Post

import UIKit

class WVCSegmentContainer : UIViewControllerOverlay {
    
    //
    // MARK: - Properties
    
    var pageViewController: WVCPageViewController? {
        didSet {
            pageViewController?.pageDelegate = self
        }
    }

    let SPBTitles = ["Dados Pessoais", "Cartão de Cidadão", "Finalizar Processo", "Registro Efectuado"]
    let service = DocumentService()
    let audit = AuditController()

    //
    // MARK: - IBOutlets
    
    @IBOutlet var viewButtons: UIStackView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var yNextButtonConstraint: NSLayoutConstraint!
    @IBOutlet var nextButton: UIButton!
    
    //
    // MARK: - IBOutlets SteppedProgressView Scheme

    @IBOutlet var SPBProgressView: SteppedProgressView!
    //Auxiliar components of SPB
    @IBOutlet var SPBBox: UIView!
    @IBOutlet var SPBStep: UILabel!
    @IBOutlet var SPBTitle: UILabel!
    @IBOutlet var SPBListButton: [UIButton]!

    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureViewController()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showNextButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //
    // MARK: - Segue for Container View

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? WVCPageViewController {
            self.pageViewController = tutorialPageViewController
        }
    }
    
    //
    // MARK: - IBActions

    @IBAction func didTapNextButton(_ sender: UIButton) {
        if pageViewController?.viewControllers?.first is WVCForm {
            postWVCForm()
        } else if pageViewController?.viewControllers?.first is WVCDocuments {
            postWVCDocuments()
        } else if pageViewController?.viewControllers?.first is WVCChat {
            pageViewController?.scrollToNextViewController()
            self.viewButtons.isHidden = true
            self.deleteProcess()
        }
    }

    @IBAction func didTapBackButton(_ sender: UIButton) {
        //self.nextButton.isUserInteractionEnabled = true
        pageViewController?.scrollToBackViewController()
    }

    //
    //MARK : ApplicationSentDelegate
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true) {}
    }

    //
    //MARK : Functions Class
    
    func configureViewController(){
        navigationController?.applyWizzioBarColors()
        audit.determineMyCurrentLocation()
        audit.determineEcraLoadedTimeStamp()
        NotificationCenter.default.addObserver(self, selector: #selector(WVCSegmentContainer.sendPictureConference(notification:)),
                                               name: NotificationSocket.SEND_PICTURE_CONF, object: nil)
    }
    
    final func showNextButton(){
        if yNextButtonConstraint.constant != 0{
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 4.0,
                           initialSpringVelocity: 10.0,
                           options: [.curveEaseInOut], animations: {
                            self.viewButtons.center = CGPoint(x: self.viewButtons.center.x, y: self.viewButtons.center.y-self.viewButtons.frame.height)
            }, completion: { (animated : Bool) in
                self.yNextButtonConstraint.constant = 0
            })
        }
    }
}
