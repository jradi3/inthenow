//
//  UIViewController+Helper.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 21/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

extension UIViewController: UIGestureRecognizerDelegate
{
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func viewWithDismissKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        tap.delegate = self;
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    func delay(seconds: Double, completion: @escaping () -> ()) {
        let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: popTime) {
            completion()
        }
    }
}

class UIViewControllerOverlay : UIViewController {
    //
    // MARK: - Properties of View Controller
    
    private let overlayView = UIView()

    func showOverlay() {
        overlayView.backgroundColor = GlobalColorSystem.refA
        overlayView.alpha = 0
        overlayView.frame = self.view.frame
        self.navigationController?.view.addSubview(overlayView)
        UIView.animate(withDuration: 0.2) { self.overlayView.alpha = 0.9 }
    }
    
    func dismissOverlay(){
        UIView.animate(withDuration: 0.2, animations: {
            self.overlayView.alpha = 0
        }) { (animated : Bool) in
            self.overlayView.removeFromSuperview()
        }
    }
}
