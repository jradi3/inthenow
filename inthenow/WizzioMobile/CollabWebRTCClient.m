//
//  CollabWebRTCClient.m
//  CollabSampleWebRTC
//
//  Created by Collab Apple on 03/01/2017.
//  Copyright © 2017 Collab Apple. All rights reserved.
//

#import "CollabWebRTCClient.h"
#import <CollabWebRTC/ConnectorSipConnector.h>
#import <CollabWebRTC/VideoCallController.h>
#import "ReactiveCocoa.h"
#import "WebRTCAccount.h"
#import "WizzioMobile-Swift.h"

@interface CollabWebRTCClient () <TakePhotoListener>

@property(nonatomic, readwrite) RACSignal *connectStateChangeSignal;

@property (nonatomic, readwrite) BOOL isConnected;
@property (nonatomic, readwrite) BOOL isTryingToConnect;
@property (nonatomic, readwrite) BOOL activateBackCamera;

@property (nonatomic, readwrite) NSLock *locker;
@property (nonatomic, strong, readwrite) ConnectorSipConnector *connectorSipConnector;
@property (nonatomic, strong, readwrite) WebRTCAccount *webRTCAccount;
@property (nonatomic, strong, readwrite) VideoCallController *response;

@property (nonatomic, strong, readwrite) RACDisposable *connectorSipConnectorSignalConnectedDisposabe;

@property (nonatomic, readwrite) PINConfirm *overlayView;
@property (nonatomic, readwrite) UITextField *pinTextField;

@property (readwrite) NSString *typePicture;

-(instancetype) init;

-(void) connectWebRTC;
-(void) disconnectWebRTC;
-(UIViewController *) createCallViewControllerInternalForNumber:(NSString *)number withCalleDisplayName:(NSString *)displayName isVideoCall:(BOOL) isVideoCall;

@end

#define LOCKER(CODE) [self.locker lock];    \
                         CODE               \
                        [self.locker unlock]

@implementation CollabWebRTCClient

#pragma mark - init

-(instancetype) init {
    if(self = [super init]) {
        self.isConnected = NO;
        self.isTryingToConnect = NO;
        self.locker = [[NSLock alloc] init];
        self.connectorSipConnector =[ConnectorSipConnector sharedInstance];
        self.connectStateChangeSignal = [RACSubject subject];
        [self registerSipConnectorEvents];
        [self addObservers];
    }
    
    return self;
}

#pragma mark -
#pragma mark - SharesInstance

+(instancetype) sharedInstance {
    static dispatch_once_t token;
    static CollabWebRTCClient *instance;
    dispatch_once(&token, ^{
        instance = [[CollabWebRTCClient alloc] init];
    });
    
    return instance;
}

#pragma mark -
#pragma mark - Public methods iplementation

-(void) registerAccount:(WebRTCAccount *)account {
    LOCKER(
           self.webRTCAccount = account;
           [self disconnectWebRTC];
           [self connectWebRTC];
    );
}

-(UIViewController *) createAudioCallViewControllerForNumber:(NSString *)number withCalleeDisplayName:(NSString *)displayName {
    return [self createCallViewControllerInternalForNumber:number withCalleDisplayName:displayName isVideoCall:NO];
}

-(UIViewController *) createVideoCallViewControllerForNumber:(NSString *)number withCalleeDisplayName:(NSString *)displayName {
    return [self createCallViewControllerInternalForNumber:number withCalleDisplayName:displayName isVideoCall:YES];
}

#pragma mark -
#pragma mark - Private methods implemantaion

-(void) connectWebRTC {
    if(self.webRTCAccount && self.webRTCAccount.sipConnectorSocketAddress) {
        self.connectorSipConnector.sipUser = self.webRTCAccount.sipUsername;
        self.connectorSipConnector.sipPassword = self.webRTCAccount.sipUsername;
        self.connectorSipConnector.initator = ([NSString stringWithFormat:@"%@@%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"processId"], self.webRTCAccount.sipDomain]);
        [self.connectorSipConnector connect:self.webRTCAccount.sipConnectorSocketAddress];
        self.isTryingToConnect = YES;
    }
}

-(void) disconnectWebRTC {
    if(self.isTryingToConnect) {
        [self.connectorSipConnector disconnect];
        self.isTryingToConnect = NO;
        self.isConnected = NO;
    }
}

-(UIViewController *) createCallViewControllerInternalForNumber:(NSString *)number withCalleDisplayName:(NSString *)displayName isVideoCall:(BOOL) isVideoCall {
    _response = nil;
    LOCKER(
           if(self.isConnected && self.connectorSipConnector.connected) {
               self.connectorSipConnector.audioOnly = !isVideoCall;
               self.connectorSipConnector.target = ([NSString stringWithFormat:@"%@@%@", number, self.webRTCAccount.sipDomain]);
               self.connectorSipConnector.targetDisplayName = displayName;
               _response = [[VideoCallController alloc] initView];
               [_response setTakePhotoDelegate:self];
           }
           );
    return _response;
}

#pragma mark -
#pragma mark - Private util methods

-(void) registerSipConnectorEvents {
    __weak CollabWebRTCClient* weakSelf = self;
    self.connectorSipConnectorSignalConnectedDisposabe = [self.connectorSipConnector.receivePackageConnectedSignal subscribeNext:^(id package) {
        CollabWebRTCClient *strongSelf = weakSelf;
        strongSelf.isConnected = [package boolValue];
        
        [((RACSubject *)strongSelf.connectStateChangeSignal) sendNext: [NSNumber numberWithBool: strongSelf.isConnected]];
        
    }];
}

#pragma mark -
#pragma mark - setTakePhotoDelegate

-(void) onPhotoCompleted:(UIImage *)photo{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:photo, @"image", _typePicture, @"type", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SEND_PICTURE_CONF" object:dict];
}

#pragma mark -
#pragma mark - dealloc

-(void) dealloc {
    [self.connectorSipConnectorSignalConnectedDisposabe dispose];
    self.connectorSipConnectorSignalConnectedDisposabe = nil;
    self.response = nil;
}

#pragma mark -
#pragma mark - Socket Events, Central Notifications and others

-(void)addObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPIN) name:@"CONFIRM_OTP" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotateCamera) name:@"ROTATE_CAMERA" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toogleFlash) name:@"LIGHT_CONTROL" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(takePicture:) name:@"TAKE_PICTURE" object:nil];
}

// Handlers from Central Notifications

-(void)rotateCamera{
    _activateBackCamera = !_activateBackCamera;
    [_response videoCallViewSwitchCamera:_activateBackCamera];
}

-(void)toogleFlash{
    [_response videoCallViewToggleFlash];
}

-(void)takePicture:(NSNotification*)notification{
    _typePicture = @"";
    NSArray *object = notification.object;
    if (object.count > 0 && object[0][0]){
        _typePicture = [object[0][0] valueForKey:@"type"];
    }
    [_response videoCallViewTakePhoto];
}

-(void)showPIN{
    
    if (_overlayView != nil) return;
    
    AppDelegate* app = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    CGRect frame = [[app window] rootViewController].view.frame;
    
    _overlayView = [[PINConfirm alloc] initWithFrame:frame];
    _overlayView.alpha = 0;
    
    _pinTextField = [[UITextField alloc] initWithFrame:CGRectMake(frame.size.width-120, 45, 100, 35)];
    _pinTextField.textAlignment = NSTextAlignmentCenter;
    _pinTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_pinTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_pinTextField setKeyboardType:UIKeyboardTypePhonePad];
    _pinTextField.backgroundColor = [UIColor whiteColor];
    
    [_response.view addSubview:_overlayView];
    [_response.view addSubview:_pinTextField];

    _overlayView.hidden = false;
    _pinTextField.hidden = false;
    [UIView animateWithDuration:0.5 animations:^{
        _overlayView.alpha = 0.85;
        _pinTextField.alpha = 0.85;
    } completion:^(BOOL finished) {
        [_pinTextField becomeFirstResponder];
    }];
}

-(void)textFieldDidChange:(UITextField *)textField{
    if (textField.text.length == 3){
        textField.text = [textField.text stringByAppendingString:@"-"];
    }
    if (textField.text.length == 7){
        [_pinTextField setUserInteractionEnabled:false];
        [_pinTextField endEditing:true];
        [_overlayView processPIN];
        [[WebSocketService shared] emitPINWithNumber:textField.text];
        [NSTimer scheduledTimerWithTimeInterval:2 repeats:false block:^(NSTimer * _Nonnull timer) {
            //[_overlayView showResult];
            [self dismissPINView];
        }];
    }
}

-(void)dismissPINView{
    [UIView animateWithDuration:0.5 animations:^{
        _pinTextField.alpha = 0;
        _overlayView.alpha = 0;
    } completion:^(BOOL finished) {
        _pinTextField.hidden = true;
        _overlayView.hidden = true;
        [_overlayView removeFromSuperview];
        [_pinTextField removeFromSuperview];
        _overlayView = nil;
        _pinTextField = nil;
    }];
}

@end
