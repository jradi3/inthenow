//
//  Audit.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 19/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import Alamofire
import CoreLocation

class AuditController : NSObject, CLLocationManagerDelegate {
    
    //
    // MARK: - Properties
    
    let service = DocumentService()
    let locationManager = CLLocationManager()
    var userLocation = CLLocation()
    private var ecraLoadedTimeStamp = String()
    
    //
    // MARK: - Public functions

    func determineMyCurrentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func determineEcraLoadedTimeStamp() {
        self.ecraLoadedTimeStamp = "\(Date().toMillis()!)"
    }
    
    //
    // MARK: - CLLocationManagerDelegate

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error locationManager : \(error)")
    }

    func post(vc : String){
        let parameters: Parameters = [
            "udid": String(describing: InfoSystem.UDID!),
            "timestamp": ecraLoadedTimeStamp,
            "lat" : "\(self.userLocation.coordinate.latitude)",
            "long" : "\(self.userLocation.coordinate.longitude)",
            "processId" : String(describing: InfoSystem.processId!),
            "ecra": vc
        ]
        service.post(parameters: parameters, endPoint: "audit", success: { (response) in
            self.locationManager.stopUpdatingLocation()
        }) { (error) in
            print("Error post Audit : \(error)")
        }
    }

}
