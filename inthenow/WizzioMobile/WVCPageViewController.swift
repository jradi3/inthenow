//
//  WVCPageViewController.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 31/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

class WVCPageViewController: UIPageViewController {
    
    //
    // MARK: - Properties
    
    weak var pageDelegate: WVCPageViewControllerDelegate?
    
    fileprivate(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [UIStoryboard(name: "WVC", bundle: nil).instantiateViewController(withIdentifier: "WVCForm"),
                UIStoryboard(name: "WVC", bundle: nil).instantiateViewController(withIdentifier: "WVCDocuments"),
                UIStoryboard(name: "WVC", bundle: nil).instantiateViewController(withIdentifier: "WVCChat"),
                UIStoryboard(name: "WVC", bundle: nil).instantiateViewController(withIdentifier: "WVCConfirm")]
    }()
    
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
    }
    
    //
    // MARK: - Configurations
    
    func configureViewController(){
        dataSource = self
        delegate = self
        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(initialViewController)
        }
        pageDelegate?.pageViewController(self,
                                         didUpdatePageCount: orderedViewControllers.count)
        removeSwipeGesture()
    }
    
    /**
     Scrolls to the next view controller.
     */
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self,
                                                        viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController)
        }
    }
    
    func scrollToBackViewController() {
        if let visibleViewController = viewControllers?.first,
            let backViewController = pageViewController(self,
                                                        viewControllerBefore: visibleViewController) {
            scrollToViewController(backViewController, direction: .reverse)
        }
    }
    
    /**
     Scrolls to the view controller at the given index. Automatically calculates
     the direction.
     
     - parameter newIndex: the new index to scroll to
     */
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(nextViewController, direction: direction)
        }
    }
    
    /**
     Scrolls to the given 'viewController' page.
     
     - parameter viewController: the view controller to show.
     */
    fileprivate func scrollToViewController(_ viewController: UIViewController,
                                            direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            // Setting the view controller programmatically does not fire
                            // any delegate methods, so we have to manually notify the
                            // 'pageDelegate' of the new index.
                            self.notifyPageDelegateOfNewIndex()
        })
    }
    
    /**
     Notifies '_pageDelegate' that the current page index was updated.
     */
    fileprivate func notifyPageDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of: firstViewController) {
            pageDelegate?.pageViewController(self,
                                             didUpdatePageIndex: index)
        }
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
}


//
// MARK: UIPageViewControllerDataSource

extension WVCPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        //Disable loop scroll
        if previousIndex == -1 {
            return nil
        }

        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        /*
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        */
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        
        //Disable loop scroll
        if nextIndex == 4 {
            return nil
        }
        
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

//
// MARK: UIPageViewControllerDelegate

protocol WVCPageViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter pageViewController: the pageViewController instance
     - parameter count: the total number of pages.
     */
    func pageViewController(_ pageViewController: WVCPageViewController,
                            didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter pageViewController: the pageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func pageViewController(_ pageViewController: WVCPageViewController,
                            didUpdatePageIndex index: Int)
    
}

extension WVCPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        notifyPageDelegateOfNewIndex()
    }
    
}
