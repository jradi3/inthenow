//
//  WVCSegmentContainer+Service.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 20/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import SwiftSpinner
import Alamofire

extension WVCSegmentContainer {

    //
    // MARK: - Form Process
    
    func postWVCForm(){
        let vc = pageViewController?.viewControllers?.first as! WVCForm
        if vc.dataSent {
            pageViewController?.scrollToNextViewController()
        } else {
            if vc.validate() {
                processForm(vc: vc)
            } else {
                self.showSpinnerError(message: "Dados não preenchido")
            }
            
        }
    }
    
    func processForm(vc : WVCForm){
        SwiftSpinner.sharedInstance.outerColor = GlobalColorSystem.refD.withAlphaComponent(0.5)
        SwiftSpinner.show("Enviando...", animated: true)
        service.post(parameters: vc.getParameters(), endPoint: "process", success: { (response) in

            //Check if has a valid processId
            if response["id"].description.characters.count == 0 {
                self.showSpinnerError(message: "Problema no envio")
                return
            }

            //Set UserDefaults for processId from Server
            UserDefaults.standard.set(response["id"].description, forKey: "processId")
            
            //Set on Server First Audit
            self.audit.post(vc: "Form")
            self.audit.determineEcraLoadedTimeStamp()
            
            //Updates
            vc.dataSent = true
            self.pageViewController?.scrollToNextViewController()
            SwiftSpinner.hide()
        }) { (error) in
            print(error)
            self.showSpinnerError(message: "Problema no envio")
        }
    }
    
    //
    // MARK: - Documents Process
    
    func postWVCDocuments(){
        let vc = pageViewController?.viewControllers?.first as! WVCDocuments
        if vc.dataSent {
            pageViewController?.scrollToNextViewController()
        } else {
            if vc.validate() {
                processDocuments(vc: vc)
            } else {
                if vc.captureSesssion.isRunning {
                    vc.stopRunning(hasTakePicture: false)
                }else {
                    self.showSpinnerError(message: "Documentos necessários")
                }
            }
        }
    }
    
    func processDocuments(vc : WVCDocuments){
        SwiftSpinner.sharedInstance.outerColor = GlobalColorSystem.refD.withAlphaComponent(0.5)
        SwiftSpinner.show("Enviando...", animated: true)
        service.post(parameters: getPhotoParameters(type: "frente", image : vc.doc1.image!), endPoint: "images", success: { (response) in
            self.service.post(parameters: self.getPhotoParameters(type: "verso", image : vc.doc2.image!), endPoint: "images", success: { (response) in
                //Set on Server Second Audit
                self.audit.post(vc: "Documents")
                self.audit.determineEcraLoadedTimeStamp()
                
                //Updates
                vc.dataSent = true
                self.pageViewController?.scrollToNextViewController()
                SwiftSpinner.hide()
            }) { (error) in
                print(error)
                self.showSpinnerError(message: "Problema no envio")
            }
        }) { (error) in
            print(error)
            self.showSpinnerError(message: "Problema no envio")
        }
    }

    func sendPictureConference(notification : Notification){
        if let _ = notification.object {
            let dict = notification.object as! Dictionary<String, Any>
            let image = dict["image"]!
            service.post(parameters: self.getPhotoParameters(type: dict["type"]! as! String, image: image as! UIImage), endPoint: "images", success: { (response) in
                WebSocketService.shared.emitImageSend(result: response["id"].rawValue as! String)
            }) { (error) in
                WebSocketService.shared.emitImageSend(result: "ERROR")
                print(error)
            }
        }
    }

    func deleteProcess(){
        let process = "process/delete/\(String(describing: InfoSystem.processId!))"
        service.delete(endPoint: process, success: { (response) in
            //Set on Server Second Audit
            self.audit.post(vc: "Final process")
        }) { (error) in
            print(error)
        }
    }

    //
    // MARK: - Spinner Scheme
    
    func showSpinnerError(message : String){
        SwiftSpinner.sharedInstance.outerColor = UIColor.red.withAlphaComponent(0.5)
        SwiftSpinner.show(message, animated: false)
        delay(seconds: 1.0, completion: {
            SwiftSpinner.hide()
        })
    }
    
    func getPhotoParameters(type: String, image : UIImage) -> Parameters {
        let pic : NSData = UIImageJPEGRepresentation(image, 0.7)! as NSData
        let image = pic.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let parameters = [
            "processId": String(describing: InfoSystem.processId!),
            "type": type,
            "base64": image
        ]
        return parameters
    }

}
