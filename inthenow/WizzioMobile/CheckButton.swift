//
//  CheckButton.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 17/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

class CheckButton : UIButton {
    
    //
    // MARK: - IBInspectable
    
    @IBInspectable open var textToColor: NSString = ""
    
    //
    // MARK: - Properties
    
    let shapeCheckLayer = CAShapeLayer()
    
    var checkPath: UIBezierPath{
        //// Color Declarations
        let color = UIColor(red: 0.133, green: 0.639, blue: 0.725, alpha: 1.000)
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        color.setStroke()
        bezierPath.lineWidth = 1
        bezierPath.stroke()
        
        //// Bezier 2 Drawing
        let bezier2Path = UIBezierPath()
        bezier2Path.move(to: CGPoint(x: 1.5, y: 11.5))
        bezier2Path.addLine(to: CGPoint(x: 8.5, y: 19.5))
        bezier2Path.addLine(to: CGPoint(x: 21.5, y: 5.5))
        bezier2Path.lineCapStyle = .round;
        
        color.setStroke()
        bezier2Path.lineWidth = 1
        bezier2Path.stroke()
        
        return bezier2Path
    }
    
    //
    // MARK: Initializer
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    //
    // MARK: Setup
    
    func setup(){
        let yPosition = frame.height/2
        shapeCheckLayer.position = CGPoint(x: frame.size.width-30, y: yPosition-12)
        shapeCheckLayer.path = checkPath.cgPath
        shapeCheckLayer.strokeColor = GlobalColorSystem.refD.cgColor
        shapeCheckLayer.fillColor = UIColor.clear.cgColor
        shapeCheckLayer.lineWidth = 2.0
        shapeCheckLayer.lineCap = kCALineCapRound
    }
    
    //
    // MARK: IBActions
    
    func tappedForCheck() {
        shapeCheckLayer.removeFromSuperlayer()
        selectedTextColor(color : GlobalColorSystem.refA)
        layer.addSublayer(shapeCheckLayer)
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0.0
        animation.byValue = 1.0
        animation.duration = 0.25
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        shapeCheckLayer.add(animation, forKey: "drawLineAnimation")
    }
    
    func selectedTextColor(color : UIColor){
        let string = NSMutableAttributedString(string: self.titleLabel!.text!)
        string.setColorForText(textToColor as String, with: color)
        self.setAttributedTitle(string, for: .normal)
    }
    
    func unselectedTextColor(){
        let string = NSMutableAttributedString(string: self.titleLabel!.text!)
        string.setColorForText(textToColor as String, with: UIColor.black)
        self.setAttributedTitle(string, for: .normal)
    }
    
}
