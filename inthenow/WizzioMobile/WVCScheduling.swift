//
//  WVCScheduling.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 21/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit
import SwiftSpinner

class WVCScheduling: UIViewController {
    
    @IBOutlet var labels: [UILabel]!
    
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationController?.applyWizzioBarColors()
        self.configureViewController()
    }
    
    //
    // MARK: - IBActions
    
    @IBAction func closeSegue(_ sender: Any) {
        self.dismiss(animated: true) {}
    }
    
    func configureViewController(){
        for label in labels {
            if label.isUserInteractionEnabled {
                label.textColor = GlobalColorSystem.refA
                let tap = UITapGestureRecognizer()
                tap.addTarget(self, action:#selector(WVCScheduling.didTap))
                tap.numberOfTapsRequired = 1
                label.addGestureRecognizer(tap)
            }
        }
    }
    
    func didTap(tap : UITapGestureRecognizer){
        let tapLabel = tap.view as! UILabel
        for label in labels {
            if label.isUserInteractionEnabled {
                label.textColor = GlobalColorSystem.refA
                label.backgroundColor = .white
                label.font = UIFont(name: "Lato-Regular", size: 15.0)
            }
        }
        tapLabel.backgroundColor = GlobalColorSystem.refJ
        tapLabel.textColor = GlobalColorSystem.refC
        tapLabel.font = UIFont(name: "Lato-Bold", size: 23.0)
    }
    
    @IBAction func schedule(_ sender: Any) {
        SwiftSpinner.show(delay: 0, title: "Agendado", animated: false)
        delay(seconds: 2.0, completion: {
            SwiftSpinner.hide()
            self.dismiss(animated: true) {}
        })
    }
    
}
