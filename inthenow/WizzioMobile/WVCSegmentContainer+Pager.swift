//
//  WVCSegmentContainer+Pager.swift
//  WizzioConference
//
//  Created by Tarek Jradi on 04/04/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

//
// MARK: - WVCPageViewControllerDelegate

extension WVCSegmentContainer: WVCPageViewControllerDelegate {
    
    func pageViewController(_ tutorialPageViewController: WVCPageViewController,
                            didUpdatePageCount count: Int) {
    }
    
    func pageViewController(_ tutorialPageViewController: WVCPageViewController,
                            didUpdatePageIndex index: Int) {
        if (SPBProgressView.hasLoad) {
            animateBoxView(for: index)
        } else if (index == 0){
            SPBProgressView.hasLoad = true
        }
    }
}

//
// MARK: - SteppedProgressView Controllers

extension WVCSegmentContainer {
    
    func animateBoxView(for index: Int){
        let isBackStep = index < SPBProgressView.currentTab ? true : false
        let flagStep : CGFloat = isBackStep ? -1 : 1
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.SPBBox.frame = CGRect(x: self.SPBBox.frame.origin.x+39*(flagStep),
                                       y: self.SPBBox.frame.origin.y,
                                       width: self.SPBBox.frame.width,
                                       height: self.SPBBox.frame.height)
        },completion: { finish in
            self.updateButton(index, hideButton: isBackStep)
        })
    }
    
    func updateButton(_ index: Int, hideButton: Bool){
        self.SPBListButton[self.SPBProgressView.currentTab].isHidden = hideButton
        self.SPBProgressView.currentTab = index
        self.SPBStep.text = "\(index+1)"
        if index < self.SPBTitles.count {
            self.SPBTitle.text = self.SPBTitles[index]
        }
    }
}
