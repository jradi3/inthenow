//
//  WVCDocuments+AVCapture.swift
//  WizzioMobile
//
//  Created by Tarek Jradi on 28/07/2017.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit
import AVFoundation

extension WVCDocuments {
    
    //Called in ViewDidLoad

    func initCaptureSession(){
        captureSesssion = AVCaptureSession()
        captureSesssion.sessionPreset = AVCaptureSessionPresetPhoto
        cameraOutput = AVCapturePhotoOutput()
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        if let input = try? AVCaptureDeviceInput(device: device) {
            if (captureSesssion.canAddInput(input)) {
                captureSesssion.addInput(input)
                if (captureSesssion.canAddOutput(cameraOutput)) {
                    captureSesssion.addOutput(cameraOutput)
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSesssion)
                    previewLayer.frame = doc1.bounds
                    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                }
            } else {
                print("issue here : captureSesssion.canAddInput")
            }
        } else {
            print("some problem here")
        }
    }
    
    //Take picture button
    
    func didPressTakePhoto(view : UIImageView) {
        //case yes, stop running, take the pickture
        //else make animation and start running.
        if captureSesssion.isRunning {
            takePicture()
            return
        }
        startRunning(view: view)
    }
    
    //Set AVCapturePhotoSettings and capture photo
    
    func takePicture(){
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 10,
            kCVPixelBufferHeightKey as String: 10
        ]
        settings.previewPhotoFormat = previewFormat
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }

    
    //CallBack from take picture
    
    func capture(_ captureOutput: AVCapturePhotoOutput,  didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?,  previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings:  AVCaptureResolvedPhotoSettings, bracketSettings:   AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let error = error {
            print("error occure : \(error.localizedDescription)")
        }
        if  let sampleBuffer = photoSampleBuffer,
//            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage =  AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer:  sampleBuffer, previewPhotoSampleBuffer: nil/*previewBuffer*/) {
            print(UIImage(data: dataImage)?.size as Any)
            
            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            let image = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImageOrientation.right)
            let vs = isDoc1ToShow ? doc1 : doc2
            stopRunning(hasTakePicture: true)
            vs?.image = cropToPreviewLayer(originalImage: image)
        } else {
            print("some error here")
        }
    }
    
    //Show elements, position view to originalTransform
    //and stop running

    func stopRunning(hasTakePicture : Bool){
        let view = isDoc1ToShow ? doc1 : doc2
        let vh = isDoc1ToShow ? doc2 : doc1
        //update views
        captureSesssion.stopRunning()
        previewLayer.removeFromSuperlayer()

        //return original position
        UIView.animate(withDuration: 0.3, animations: {
            view?.transform = self.originalTransform
            view?.center = self.originalCenter
        }, completion: { (complet) in
            //update labels and views
            vh?.isHidden = false
            self.titleConfirmBack.isHidden = false
            self.titleConfirmFront.isHidden = false
            if hasTakePicture {
                if self.isDoc1ToShow {
                    self.titleConfirmFront.titleLabel?.text = "FRENTE "
                    self.titleConfirmFront.tappedForCheck()
                } else {
                    self.titleConfirmBack.titleLabel?.text = "VERSO "
                    self.titleConfirmBack.tappedForCheck()
                }
            }
        })
    }

    //Hide elements, position view to show at center view
    //and start running
    
    func startRunning(view : UIImageView){
        let vh = isDoc1ToShow ? doc2 : doc1
        vh?.isHidden = true
        titleConfirmBack.isHidden = true
        titleConfirmFront.isHidden = true
        UIView.animate(withDuration: 0.3) {
            vh?.isHidden = true
            self.originalTransform = view.transform
            self.originalCenter = view.center
            view.transform = view.transform.scaledBy(x: 1.3, y: 1.3);
            view.center = CGPoint(x: self.view.center.x, y: self.view.center.y-30)
        }
        view.layer.addSublayer(previewLayer)
        captureSesssion.startRunning()
    }
    
    // This method you can use somewhere you need to know camera permission   state
    
    func askPermission() {
        print("here")
        let cameraPermissionStatus =  AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        
        switch cameraPermissionStatus {
        case .authorized:
            print("Already Authorized")
        case .denied:
            print("denied")
            
            let alert = UIAlertController(title: "Sorry :(" , message: "But  could you please grant permission for camera within device settings",  preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel,  handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
        case .restricted:
            print("restricted")
        default:
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {
                [weak self]
                (granted :Bool) -> Void in
                
                if granted == true {
                    // User granted
                    print("User granted")
                    DispatchQueue.main.async(){
                        //Do smth that you need in main thread
                    }
                }
                else {
                    // User Rejected
                    print("User Rejected")
                    
                    DispatchQueue.main.async(){
                        let alert = UIAlertController(title: "WHY?" , message:  "Camera it is the main feature of our application", preferredStyle: .alert)
                        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                        alert.addAction(action)
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
            });
        }
    }
    
    private func cropToPreviewLayer(originalImage: UIImage) -> UIImage {
        let outputRect = previewLayer.metadataOutputRectOfInterest(for: previewLayer.bounds)
        var cgImage = originalImage.cgImage!
        let width = CGFloat(cgImage.width)
        let height = CGFloat(cgImage.height)
        let cropRect = CGRect(x: outputRect.origin.x * width, y: outputRect.origin.y * height, width: outputRect.size.width * width, height: outputRect.size.height * height)
        cgImage = cgImage.cropping(to: cropRect)!
        let croppedUIImage = UIImage(cgImage: cgImage, scale: 1.0, orientation: .right)
        return croppedUIImage
    }
    
}
