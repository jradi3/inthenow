//
//  LoginViewController.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 30/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import UIKit

class LoginViewController : UIViewControllerOverlay {
    
    //
    // MARK: - IBOutlets

    @IBOutlet var logo: UILabel!
    @IBOutlet var touchId: UIImageView!
    
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        // Do any additional setup after loading the view, typically from a nib.
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (timer) in self.showElements() }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //
    // MARK: - Class functions
    
    func showElements(){
        UIView.animate(withDuration: 0.5, delay: 0.5, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.logo.center = CGPoint(x: self.logo.center.x, y: self.logo.center.y-70)
        },completion: { finish in
            UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseOut,animations: {
                for view in self.view.subviews {
                    view.alpha = 1.0
                }
            },completion: nil)
        })
    }
    
    //
    // MARK: - IBActions
    
    @IBAction func authenticate(sender : UIButton){
        sender.shake()
        //appDelegate.switchLoginSuccessful()
    }

}

