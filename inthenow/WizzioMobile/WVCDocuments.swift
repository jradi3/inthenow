//
//  WVCDocuments.swift
//  WizzioConference
//
//  Created by Tarek Abdala on 28/03/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//
//  WVC Wizzio Video Conference

import UIKit
import AVFoundation

class WVCDocuments: UIViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    AVCapturePhotoCaptureDelegate{

    //
    // MARK: - Properties

    var dataSent = false
    var isDoc1ToShow = true
    let imagePick = UIImagePickerController()
    var originalTransform = CGAffineTransform()
    var originalCenter = CGPoint()

    //
    // MARK: - AVCapture

    var captureSesssion : AVCaptureSession!
    var cameraOutput : AVCapturePhotoOutput!
    var previewLayer : AVCaptureVideoPreviewLayer!
    
    //
    // MARK: - IBOutlets

    @IBOutlet weak var titleConfirmFront: CheckButton!
    @IBOutlet weak var titleConfirmBack: CheckButton!
    @IBOutlet weak var doc1: UIImageView!
    @IBOutlet weak var doc2: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!

    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        askPermission()
        initCaptureSession()
    }
    
    //
    // MARK: - IBActions
    
    @IBAction final func openCamera(_ sender: UITapGestureRecognizer){
        let view = sender.view as! UIImageView
        isDoc1ToShow = view.restorationIdentifier == "doc1" ? true : false
        self.didPressTakePhoto(view: view)
    }
    
    // MARK: UIDocumentInteractionControllerDelegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    // MARK: Public functions

    func validate() -> Bool {        
        if (captureSesssion.isRunning ||
            titleConfirmFront.titleLabel?.text != "FRENTE " ||
            titleConfirmBack.titleLabel?.text != "VERSO "){
            return false
        }
        return true
    }

}
