//
//  DataSingleton.swift
//  inthenow
//
//  Created by Tarek Jradi on 01/09/2017.
//  Copyright © 2017 Genial Tech. All rights reserved.
//

class DataSingleton {
    
    //MARK: Shared Instance
    
    static let shared : DataSingleton = {
        let instance = DataSingleton(array: [])
        return instance
    }()
    
    //MARK: Local Variable
    
    var people = [Any]()
    var phoneNumber = String()
    
    //MARK: Init
    
    init( array : [Any]) {
        people = array
    }
}

