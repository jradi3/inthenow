//
//  PersonService.swift
//  inthenow
//
//  Created by Tarek Jradi on 27/08/2017.
//  Copyright © 2017 Tarek Jradi. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON

public class PersonService : Service {
    
    //
    // MARK: - .GET (Functions Data Request)
    
    /**
     get a list of people
     
     - returns: Result<Person>
     */
    
    func getProfiles(success : @escaping (Array<Person>) -> (), failure  : ((_ error : Error) -> ())?) {
        Service.manager.request("\(APIUrls.prefixMockable)inthenow/people", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject { (response:DataResponse<PersonResponse>) in
            if let result = response.result.value {
                success(result.list!)
            }
        }
    }
    
    func get(   endPoint : String,
                success: @escaping ((_ result : JSON) -> ()),
                failure: @escaping ((_ error : Error) -> ())){
        
        var urlRequest = URLRequest(url: URL(string: "\(APIUrls.prefix)\(endPoint)")!)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        
        var headers: HTTPHeaders
        if let existingHeaders = urlRequest.allHTTPHeaderFields {
            headers = existingHeaders
        } else {
            headers = HTTPHeaders()
        }
        headers["Authorization"] = UserDefaults.standard.string(forKey: "token")!
        urlRequest.allHTTPHeaderFields = headers
        
        Alamofire.request(urlRequest)
            .responseJSON { response in
                success(JSON(response.result.value!))
        }

        
    }
    
    
    func post(parameters: Parameters, endPoint : String,
              success: @escaping ((_ result : JSON) -> ()),
              failure: @escaping ((_ error : Error) -> ())){
        Alamofire.request("\(APIUrls.prefix)\(endPoint)", method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default).responseJSON { (response) in
                            if !(response.result.value is NSNull) {
                                if response.result.error != nil {
                                    failure(response.result.error!)
                                } else {
                                    success(JSON(response.result.value!))
                                }
                            }
        }
    }
    
}
