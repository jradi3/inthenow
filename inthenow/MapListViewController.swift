//
//  MapListViewController.swift
//  inthenow
//
//  Created by Tarek Jradi on 01/09/2017.
//  Copyright © 2017 Genial Tech. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Kingfisher

class MapListViewController : UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet var mapView: MKMapView!
    
    
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(MapListViewController.addPins), userInfo: nil, repeats: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        determineCurrentLocation()
    }
    
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        manager.stopUpdatingLocation()
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008))
        
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        myAnnotation.title = "-1"
        mapView.addAnnotation(myAnnotation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func addPins() {
//        let nePoint = CGPoint(x: mapView.bounds.origin.x + mapView.bounds.size.width, y: mapView.bounds.origin.y)
//        let sePoint = CGPoint(x: mapView.bounds.origin.x, y: mapView.bounds.origin.y + mapView.bounds.size.height)
//        
//        let neCoord = mapView.convert(nePoint, toCoordinateFrom: mapView)
//        let seCoord = mapView.convert(sePoint, toCoordinateFrom: mapView)
//        
//        var y = DataSingleton.shared.people.count
//        
//        while y > 0 {
//            let latRange = randomBetweenNumbers(firstNum: Float(neCoord.latitude), secondNum: Float(seCoord.latitude))
//            let longRange = randomBetweenNumbers(firstNum: Float(neCoord.longitude), secondNum: Float(seCoord.longitude))
//            let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(latRange), longitude: CLLocationDegrees(longRange))
//            let pin = MKPointAnnotation()
//            pin.coordinate = location
//            pin.title = "\(y)"
//            
//            
//            
//            
//            mapView.addAnnotation(pin)
//            y -= 1
//        }
        var y = 0
        for value in DataSingleton.shared.people {
            let person = value as! Person
            let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(person.lat!), longitude: CLLocationDegrees(person.lng!))
            let pin = MKPointAnnotation()
            pin.coordinate = location
            pin.title = "\(y)"
            mapView.addAnnotation(pin)
            y += 1
        }
        
        
    }
    
    func randomBetweenNumbers(firstNum: Float, secondNum: Float) -> Float{
        return Float(arc4random()) / Float(UINT32_MAX) * abs(firstNum - secondNum) + min(firstNum, secondNum)
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
    }
 

    func didDragMap(gestureRecognizer: UIGestureRecognizer) {
        print("Drag")
    }
    
    private var mapChangedFromUserInteraction = false

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if (mapChangedFromUserInteraction) {
            print("ZOOM finished")
        }
    }
    
    private func mapViewRegionDidChangeFromUserInteraction() -> Bool {
        let view = self.mapView.subviews[0]
        if let gestureRecognizers = view.gestureRecognizers {
            for recognizer in gestureRecognizers {
                if( recognizer.state == UIGestureRecognizerState.began || recognizer.state == UIGestureRecognizerState.ended ) {
                    return true
                }
            }
        }
        return false
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            
            let index = Int(annotation.title!!)!
            
            print("latitude : \(annotation.coordinate.latitude)")
            print("longitude : \(annotation.coordinate.longitude)")
            
            if index == -1 {
                let label = UILabel()
                label.text = "You"
                label.textAlignment = NSTextAlignment.center
                label.font = UIFont.boldSystemFont(ofSize: 12.0)
                label.textColor = .red
                annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: 75, height: 75)
                annotationView.layer.cornerRadius = annotationView.frame.size.height/2
                annotationView.layer.masksToBounds = true
                annotationView.layer.borderColor = UIColor.white.cgColor
                annotationView.layer.borderWidth = 5
                annotationView.backgroundColor = .white
                label.frame = annotationView.frame
                annotationView.addSubview(label)
                annotationView.layer.zPosition = 0
                return annotationView
            }
            
            let p = DataSingleton.shared.people[index] as! Person
            let url = URL(string: p.imageUrl!)

                annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: 0, height: 0)
            
            
            if index == -1 {
                let label = UILabel()
                label.text = "You"
                label.textAlignment = NSTextAlignment.center
                label.font = UIFont.boldSystemFont(ofSize: 12.0)
                label.textColor = .red
                annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: 75, height: 75)
                annotationView.layer.cornerRadius = annotationView.frame.size.height/2
                annotationView.layer.masksToBounds = true
                annotationView.layer.borderColor = UIColor.white.cgColor
                annotationView.layer.borderWidth = 5
                annotationView.backgroundColor = .white
                label.frame = annotationView.frame
                annotationView.addSubview(label)
                annotationView.layer.zPosition = 0
            }
            
            if index != -1 {
                
                ImageDownloader.default.downloadImage(with: url!, options: [], progressBlock: nil) {
                    (image, error, url, data) in
                    annotationView.image = image
                    annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: 55, height: 55)
                    annotationView.layer.cornerRadius = annotationView.frame.size.height/2
                    annotationView.layer.masksToBounds = true
                    annotationView.layer.borderColor = UIColor.white.cgColor
                    annotationView.layer.borderWidth = 5
                    annotationView.backgroundColor = .white
                    annotationView.canShowCallout = false
                    annotationView.contentMode = UIViewContentMode.scaleAspectFit
                    annotationView.layer.zPosition = -1


                    }
            }
            

            
            annotationView.canShowCallout = false
            annotationView.contentMode = UIViewContentMode.scaleAspectFit

            
            
        }
        
        return annotationView
    }


}
