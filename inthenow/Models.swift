//
//  Models.swift
//  OLXChallenge
//
//  Created by Tarek Abdala on 12/02/17.
//  Copyright © 2017 Tarek Abdala. All rights reserved.
//

import Foundation

extension Person {
    /**
     This method format a image path hosted of OLX server. This path include: base_url_image, 
     id of list images, index of image, name of product and jpg extension
     
     - parameter category: Index of image
     - return: image path
     */
    func getURLImage(index:Int) -> String{
        let path = "\(APIUrls.base_url_image)\(String(describing: self.imageUrl!))"
        return path
    }
}
