//
//  PersonViewController.swift
//  inthenow
//
//  Created by Tarek Jradi on 31/08/2017.
//  Copyright © 2017 Genial Tech. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource {

    //
    // MARK: - IBOutlets
    
    @IBOutlet var inviteButton: UIButton!
    @IBOutlet var yNextButtonConstraint: NSLayoutConstraint!
    @IBOutlet var tableView : UITableView!

    //
    // MARK: - Properties
    
    let cellIdentifier = "PersonListTableViewCell"
    //
    // MARK: - ViewController life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showNextButton()
    }
    
    // MARK: - Segues

    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! ProductPageViewController
                controller.objects = objects
                controller.selectedIndex = indexPath.row
            }
        }
    }
    */
 
    //
    // MARK: - Configurations
    
    func configureView() {
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        inviteButton.layer.cornerRadius = 23.0
    }

    //
    //  MARK: - DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSingleton.shared.people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonListTableViewCell", for: indexPath) as! PersonListTableViewCell
        let p = DataSingleton.shared.people[indexPath.row] as! Person
        cell.bind(with: p)
        return cell
    }
    
    //
    //  MARK: - Delegate
    /*
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: nil)
    }
    */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }

    final func showNextButton(){
        if yNextButtonConstraint.constant != 20{
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 4.0,
                           initialSpringVelocity: 10.0,
                           options: [.curveEaseInOut], animations: {
                            self.inviteButton.center = CGPoint(x: self.inviteButton.center.x, y: self.inviteButton.center.y-(self.inviteButton.frame.height+20))
            }, completion: { (animated : Bool) in
                self.yNextButtonConstraint.constant = 20
            })
        }
    }

}
